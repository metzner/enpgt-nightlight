# ENPGT Nightlight Tool

_Part of the "EnvNeuro Python GIS Toolbox", a Python project for extracting multiple types of geodata from large datasets, for lists of coordinates from participants in EnvNeuro studies. This overarching project is currently in early theoretical stages, although individual tools like this one are already in a usable standalone state._

This is a tool that, for a given input of coordinates in Europe, calculates the average amount of Night Light using [this dataset](https://eogdata.mines.edu/nighttime_light/).

Usage:
1. Install Python requirements (Python 3 is required)
2. Make sure you have a task file inside the root directory. The support here is currently specifically tailored towards one dataset, others may or may not work. Your dataset must be in WGS84. Make sure it is comma seperated, and that the topmost line has labels for the columns, ideally "id", "lat", "lng".
3. Run `python3 main.py [filename] [radius]`. If you don't specify a radius, it'll do 100, 200, 500, 1000 and 2000 in succession.
4. When using larger radii like 2000m, bring some patience as the calculations take somewhere betweem 0.1s to 1s per data point, depending on the speed of your computer.

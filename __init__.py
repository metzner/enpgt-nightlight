import importlib.util
import sys
import os.path
import logging
from dataclasses import dataclass, field
from typing import List

from pyproj import Transformer

from BaseClasses import ToolDefinition, ToolDefinitionArgs

sys.path.append(os.path.dirname(os.path.realpath(__file__)))

modname = 'Nightlight_Mod'
mod_dir = os.path.dirname(os.path.realpath(__file__))
fname = str(os.path.join(mod_dir, "main.py"))

spec = importlib.util.spec_from_file_location(modname, fname)
if spec is None:
    raise ImportError(f"Could not load spec for module '{modname}' at: {fname}")
module = importlib.util.module_from_spec(spec)
sys.modules[modname] = module
try:
    spec.loader.exec_module(module)
except FileNotFoundError as e:
    raise ImportError(f"{e.strerror}: {fname}") from e


class NightLightToolClass(ToolDefinition):
    tool_name: str = "Night Light Tool"
    metric_name: str = "Night Light"

    transproj = Transformer.from_crs(
        "WGS84",
        "EPSG:3035",
        always_xy=True,
    )

    @dataclass
    class NightLightToolArgs(ToolDefinitionArgs):
        radii: List[int] = field(default_factory=lambda: [100, 200, 500, 1000, 2000])

    @classmethod
    def supports_coordinate(cls, lat: float, lng: float) -> bool:
        return -75 < lat < 75

    @classmethod
    def required_setup(cls):
        logging.warning("Automatic setup for NightLightTool has not been implemented.")
        return

    @classmethod
    def main(cls, args: NightLightToolArgs) -> List[str]:
        sysargs = [str(args.radii).replace(" ", ""), os.path.realpath(args.task_file_location)]
        correct_dir = os.getcwd()

        tool_dir = os.path.dirname(os.path.realpath(__file__))

        output_directories_before = []
        if os.path.isdir(os.path.join(tool_dir, "outputs")):
            output_directories_before = [
                os.path.join(tool_dir, "outputs", p) for p in os.listdir(os.path.join(tool_dir, "outputs"))
            ]

        os.chdir(tool_dir)

        module.main(sysargs)

        os.chdir(correct_dir)

        output_directories_after = []
        if os.path.isdir(os.path.join(tool_dir, "outputs")):
            output_directories_after = [
                os.path.join(tool_dir, "outputs", p) for p in os.listdir(os.path.join(tool_dir, "outputs"))
            ]

        return list(set(output_directories_after) - set(output_directories_before))

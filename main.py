import ast
import gzip
import hashlib
import math
import re
import os
import shutil
import sys
import numpy as np
import pyproj
import requests
import tqdm
import shapely.ops
import geopandas as gpd
import matplotlib.pyplot as plt
import socket

from requests.adapters import HTTPAdapter
from shapely import geometry, Polygon
from geotiff import GeoTiff
from collections import defaultdict
from datetime import datetime
from functools import lru_cache
from io import BytesIO
from multiprocessing import Pool
from pathlib import Path
from typing import List
from urllib3 import Retry

socket.setdefaulttimeout(60)
BUFFER_RADIUS = 500
BEST_YEAR = 2022

metric_descriptions = defaultdict(lambda: "Unknown")
metric_descriptions.update({
    "average": "Average monthly radiance, nW/cm2/sr",
    "average_masked": "Average monthly radiance w/ background masked, nW/cm2/sr",
    "cf_cvg": "Count of cloud free coverage",
    "cvg": "Count of coverage",
    "lit_mask": "Background light mask",
    "maximum": "Maximum monthly radiance, nW/cm2/sr",
    "median": "Median monthly radiance, nW/cm2/sr",
    "median_masked": "Median monthly radiance w/ background masked, nW/cm2/sr",
    "minimum": "Minimum monthly radiance, nW/cm2/sr",
})

def download(url: str, fname: str, session: requests.Session):
    resp = session.get(url, stream=True)
    total = int(resp.headers.get('content-length', 0))
    # Can also replace 'file' with a io.BytesIO object
    with open(fname, 'wb') as file, tqdm.tqdm(
        total=total,
        unit='iB',
        unit_scale=True,
        unit_divisor=1024,
    ) as bar:
        for data in resp.iter_content(chunk_size=1024):
            size = file.write(data)
            bar.update(size)


def weighted_avg_and_std(values, weights):
    """
    Return the weighted average and standard deviation.

    values, weights -- NumPy ndarrays with the same shape.
    """
    average = np.average(values, weights=weights)
    # Fast and numerically precise:
    variance = np.average((values - average) ** 2, weights=weights)
    return average, math.sqrt(variance)


@lru_cache
def get_image(desired_year: int, tif_type: str):
    retry = Retry(total=200, backoff_factor=0.1)
    adapter = HTTPAdapter(max_retries=retry)

    session = requests.Session()
    session.mount('https://', adapter)

    dir_url = "https://eogdata.mines.edu/nighttime_light/annual/v22/2022/"

    local_filename = hashlib.md5(dir_url.encode('utf-8')).hexdigest()

    response = session.get(dir_url)
    if response.status_code == requests.codes.ok:
        file = BytesIO(response.content)
        with open(local_filename, 'wb') as output:
            output.write(file.getbuffer().tobytes())

    found_file = None

    for _ in range(10):
        with open(local_filename) as html:
            content = html.read()

            z = set(re.findall(r"VNL[^>]*" + re.escape(tif_type) + r".dat.tif.gz", content))

            for file in z:
                found_file = file

        if found_file:
            break
    else:
        assert found_file, f"File not found: {desired_year}, {tif_type}"

    file_url = dir_url + found_file

    place_to_put_it = os.path.join("download", found_file)
    extract_file = os.path.join("download", Path(found_file).stem)

    file_exists = True

    if not os.path.isfile(extract_file):
        print(f"File {extract_file} isn't already downloaded. It will be downloaded now. This may take a while.")
        file_exists = False

    else:
        try:
            get_geo_tiff(extract_file)
        except Exception:
            print(f"File {extract_file} is corrupted. Redownloading. This may take a while.")
            file_exists = False

    if not file_exists:
        if not os.path.isfile(place_to_put_it):
            download(file_url, place_to_put_it, session)

        with gzip.open(place_to_put_it, 'rb') as f_in:
            with open(extract_file, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)

    if os.path.isfile(place_to_put_it):
        os.remove(place_to_put_it)

    assert os.path.isfile(extract_file), "File couldn't be found at all even after extracting"

    return extract_file


@lru_cache(maxsize=2)
def get_geo_tiff(file: str) -> GeoTiff:
    return GeoTiff(file)


def do_single_nightlight(geo_id: str, lat: float, lng: float, desired_radius: int, tif_type: str, plot=False):
    file = get_image(BEST_YEAR, tif_type)

    geo_tiff = get_geo_tiff(file)

    max_y, max_x = geo_tiff.tif_shape

    # get radius polygon
    radius_poly = geometry.Point(0, 0).buffer(desired_radius, resolution=40)
    buffer_poly = geometry.Point(0, 0).buffer(desired_radius + BUFFER_RADIUS, resolution=40)
    crs_aeqd = pyproj.Proj(proj='aeqd', datum='WGS84', lon_0=lng, lat_0=lat, units='m')
    crs_wgs84 = pyproj.Proj("WGS84")
    transproj = pyproj.transformer.Transformer.from_proj(crs_aeqd, crs_wgs84, always_xy=True)
    transproj_reverse = pyproj.transformer.Transformer.from_proj(crs_wgs84, crs_aeqd, always_xy=True)

    buffer_poly_in_wgs84 = shapely.ops.transform(transproj.transform, buffer_poly)

    buffer_min_x, buffer_min_y, buffer_max_x, buffer_max_y = buffer_poly_in_wgs84.bounds
    min_x_pixel = geo_tiff._get_x_int(buffer_min_x)
    max_y_pixel = geo_tiff._get_y_int(buffer_min_y)  # y is in reverse
    max_x_pixel = geo_tiff._get_x_int(buffer_max_x)
    min_y_pixel = geo_tiff._get_y_int(buffer_max_y)  # y is in reverse

    pixel_polys = []
    pixel_values = dict()

    geotiff_array = geo_tiff.read()

    for x in range(min_x_pixel, max_x_pixel + 1):
        for y in range(min_y_pixel, max_y_pixel + 1):
            if x > max_x or x < 0 or y > max_y or y < 0:
                raise ValueError("x and y went over the borders of an image. There is no handling for this yet")
            left_top = tuple(geo_tiff.get_coords(x, y))
            right_bottom = tuple(geo_tiff.get_coords(x + 1, y + 1))
            right_top = (right_bottom[0], left_top[1])
            left_bottom = (left_top[0], right_bottom[1])

            poly = shapely.ops.transform(transproj_reverse.transform, Polygon([left_top, right_top, right_bottom, left_bottom]))
            poly = shapely.intersection(poly, radius_poly)
            poly = shapely.ops.transform(transproj.transform, poly)
            if not shapely.is_empty(poly):
                pixel_polys.append(poly)
                pixel_values[left_top] = geotiff_array[y][x]

    gdf = gpd.GeoDataFrame(index=list(pixel_values.keys()), crs=crs_wgs84.crs, geometry=pixel_polys)
    gdf = gdf.to_crs({'proj': 'cea'})

    pixel_areas = gdf["geometry"].area.to_dict()

    assert list(pixel_areas) == list(pixel_values)

    if plot:
        gdf.plot(cmap="Set1")
        plt.show()

    mean, stddev = weighted_avg_and_std(np.array(list(pixel_values.values())), np.array(list(pixel_areas.values())))

    mean = round(mean, 5)
    stddev = round(stddev, 5)
    resolution = max(math.sqrt(area) for area in pixel_areas.values())

    if len(pixel_values) < 2:
        stddev = "None"

    return geo_id, mean, stddev, len(pixel_values), resolution, tif_type

# adding 0.001 to the coord for max_y_pixel makes it the previous pixel.
# subtracting 0.001 to the coord for max_x_pixel makes it the previous pixel.


def do_single_nightlight_star(star):
    return do_single_nightlight(*star)


def do_nightlight(infile: str, output_dir: str, radius: int, types: List[str]):
    print("---")
    print(f"Getting nightlight stats for file {infile} with radius {radius}m.")

    shutil.copyfile(infile, os.path.join(output_dir, os.path.basename(infile)))

    id_lat_lngs = []
    id_to_lat_lng = {}

    with open(infile) as task_file:
        for line in task_file.readlines():
            line = line.strip()
            line_split = re.split('[;,\\t]', line)
            try:
                id_lat_lngs.append((line_split[0], float(line_split[1]), float(line_split[2])))
                id_to_lat_lng[line_split[0]] = (float(line_split[1]), float(line_split[2]))
            except Exception:
                print(f"Could not interpet line: {line}")

    print("Pre-download all the files in preparation of multiprocessing")
    # Pre-download all the files before doing multiprocessing

    starmap = []
    for tif_type in types:
        get_image(2022, tif_type)
        starmap += [(geo_id, lat, lng, radius, tif_type) for geo_id, lat, lng in id_lat_lngs]

    with Pool() as pool:
        output = list(tqdm.tqdm(pool.imap(do_single_nightlight_star, starmap), total=len(starmap)))

    output_dict = defaultdict(lambda: dict())
    pixels_and_resolution = dict()

    for id_values in output:
        output_dict[id_values[5]][id_values[0]] = id_values[1:3]
        pixels_and_resolution[id_values[0]] = id_values[3:5]

    with open(os.path.join(output_dir, f"output_{radius}.csv"), "w") as output_file:
        with open(infile) as task_file:
            output_file.write("id,lat,lng,amtOfPixels,resolutionInMeters,")
            output_file.write(",".join(f"{tif_type}_mean,{tif_type}_stddev" for tif_type in types))
            output_file.write("\n")

            for line in task_file.readlines():
                line = line.strip()
                line_split = re.split('[;,\\t]', line)

                geo_id = line_split[0]

                if geo_id not in pixels_and_resolution:
                    continue

                lat, lng = id_to_lat_lng[geo_id]
                pixel_amt, resolution = pixels_and_resolution[geo_id][0], pixels_and_resolution[geo_id][1]

                values = ""
                for tif_type in types:
                    values += ","
                    values += ",".join(str(token) for token in output_dict[tif_type][geo_id])

                output_file.write(f"{geo_id},{lat},{lng},{pixel_amt},{resolution}{values}\n")


def output_readme(file_path: str, radii: List[int], types: List[str]):
    with open(file_path, "w") as f:
        f.write("NIGHT LIGHT TOOL\n\n"
                "This file contains some information on how the data was generated and how to interpret it.")

        f.write("\n\n-----\n\n"
                "NIGHT LIGHT DATASET\n\n"
                f"This tool uses the Earth Observation Group's VIIRS Night Light dataset from {BEST_YEAR}.\n"
                "This dataset consist of image files where each pixel corresponds to a 15*15 arc-second area.\n"
                "This dataset has multiple image files per year for different metrics."
                " The best way to understand these is to read up on them here:\n"
                "https://eogdata.mines.edu/products/vnl/#annual_v2.")

        f.write("\n\n-----\n\n"
                "INPUT AND OUTPUT\n\n"
                "Each line in the task file defines a location.\n"
                "The Night Light Tool will extract and average out the night light values"
                " in a radius around each location.\n\n")

        radius_text = (f"In this case, the radius {radii[0]}m was used."
                       f" The output can be viewed in \"output_{radii[0]}.csv\".")
        if len(radii) > 1:
            radius_text = (f"In this case, the radii {', '.join((str(radius) for radius in radii[:-1]))}"
                           f" and {radii[-1]} were used. For each radius, you'll be able to find an output file, for"
                           f" example: \"output_{radii[0]}.csv\".")

        f.write(radius_text + "\n")

        f.write("An output file usually contains the id, lat and lng of each location, followed by the Night"
                " Light values calculated by the tool.")

        f.write("\n\n-----\n\n"
                "WHAT EXACTLY WAS DONE FOR THIS TASK?\n\n")

        f.write("The amount of used pixels can be seen in the output parameter \"amtOfPixels\"in the output file.\n\n")

        f.write("Since this dataset uses a basic equirectangular projection, there is a lot of distortion.\n"
                "Specifically, each pixel represents a different area depending on its latitude.\n"
                "The average resolution of the pixels in meters around each location can be found in the \"resolution\""
                "parameter in the output file.\n"
                "This is a unit of distance, which was simply calculated as the square root of the area of the pixel."
                "The pixels are not actually square, they are usually vertical rectangles.\n\n")

        f.write("After collecting all the pixels in a radius around a location, an arithmetic mean and standard"
                " deviation are calculated for each metric provided.\n"
                "If one of the pixel rectangles only partially falls within the radius"
                ", it is only partially considered. (weighted mean & stddev by area within the radius)\n")

        f.write("\n-----\n\n")
        f.write("METRICS\n\n")

        f.write("In this case, the following metrics were used:\n\n")

        for metric in types:
            f.write(f"{metric}: {metric_descriptions[metric]}\n")

        f.write("\nFor each metric, you will find a mean and a standard deviation in the output file.\n"
                f"For example, for the {types[0]} metric, you will find {types[0]}_mean and {types[0]}_stddev.\n\n")

        confusing_metrics = [metric for metric in ["average", "median", "minimum", "maximum"] if metric in types]

        if confusing_metrics:
            confusing_metric = confusing_metrics[0]
            f.write(f"This can be a bit confusing in the case of something like \"{confusing_metric}_mean\".\n"
                    f"To be clear, this means that each pixel in the dataset represents the {confusing_metric} night"
                    f" light across the year ({confusing_metric} = \"yearly {confusing_metric} night light\""
                    " per pixel).\n"
                    "What was calculated by the tool is the average value of the pixels that are in the specified"
                    " radius (mean = \"mean of the pixel values\").\n\n")

        f.write("To see exactly what each metric means, it is recommended to read the documentation of the dataset:\n"
                " https://eogdata.mines.edu/products/vnl/#annual_v2")


def main(args):
    default_task_file = "task.txt"

    radii = [100, 200, 500, 1000, 2000]
    types = ["average", "average_masked", "cf_cvg", "cvg", "lit_mask", "maximum", "median", "median_masked", "minimum"]

    for arg in args:
        if arg.startswith("[") and arg.endswith("]"):
            radii = ast.literal_eval(arg)

        elif arg.isnumeric():
            radii = [int(arg)]

        elif os.path.isfile(arg):
            default_task_file = arg

        else:
            print(f"Could not find file {arg}.")
            return -1

    output_dir = os.path.join("outputs", datetime.now().strftime("%d-%m-%Y_%H-%M-%S"))
    os.makedirs(output_dir)

    if not os.path.isdir("download"):
        os.makedirs(os.path.realpath("download"))

    for radius in radii:
        do_nightlight(default_task_file, output_dir, radius, types)

    output_readme(os.path.join(output_dir, "readme.txt"), radii, types)


if __name__ == "__main__":
    main(sys.argv[1:])
